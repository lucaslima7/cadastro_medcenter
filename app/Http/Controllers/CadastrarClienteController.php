<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CadastrarClienteController extends Controller
{
    public function index()
    {
        $cadastrar = new \App\Cadastrar();
        $listar = $cadastrar->all();

        return view('paginas.listar', ['listar' => $listar]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paginas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();
        $cadastrar = new \App\Cadastrar;
        $cadastrar->create($dados);

        return redirect('listar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cadastrar = new \App\Cadastrar;
        $cadastrar = $cadastrar->find($id);

        return view('paginas.ver', ['cadastrar' => $cadastrar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cadastrar = new \App\Cadastrar;
        $cadastrar = $cadastrar->find($id);

        return view('paginas.editar', ['cadastrar' => $cadastrar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cadastrar = new \App\Cadastrar;
        $cadastrar = $cadastrar->find($id)->update($request->all());
//        return redirect()->route('book.index');
        return redirect('listar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $cadastrar = new \App\Cadastrar;
        $cadastrar->find($id)->delete();

        return redirect('listar');
    }
}
