<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', "InicioController@index");

Route::post('/cadastrar/cliente', ['as'=>'cadastrar.store', 'uses'=> 'CadastrarClienteController@store']);

//Route::get('teste',function(){
//   return "cadastrado";
//});

Route::get('/listar', "CadastrarClienteController@index");

Route::get('/delete/{id}', "CadastrarClienteController@delete");
Route::get('/edit/{id}', "CadastrarClienteController@edit");
Route::get('/show/{id}', "CadastrarClienteController@show");
Route::post("/cadastrar/update/{id}", ['as'=>'cadastrar.update', 'uses'=> 'CadastrarClienteController@update']);