<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cadastrar extends Model
{
    //permitir que o laravel passar os dados em massa para o BD - MassAssigment
    protected $table = 'clientes';
    protected $fillable = [
        'name',
        'data',
        'email',
        'endereco',
        'cidade',
        'estado',
        'pais',
        'cpf',
        'rg'
    ];
}
