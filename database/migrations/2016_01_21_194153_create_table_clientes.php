<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $tabela) {
            $tabela->increments('id');
            $tabela->string('name');
            $tabela->date('data');
            $tabela->string('email');
            $tabela->string('endereco');
            $tabela->string('cidade');
            $tabela->string('estado');
            $tabela->string('pais');
            $tabela->string('cpf', 11);
            $tabela->string('rg', 15);
            $tabela->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
