@extends('layout')

@section('conteudo')
    <form method="post" action="/cadastrar/update/{{$cadastrar->id}}" class="col-sm-12 form-horizontal main_contato" role="form">
        {!! Form::model($cadastrar, ['route'=>['cadastrar.update', $cadastrar->id]]) !!}
    <div class="black-text">Formulário de Cadastro</div>
    <br>

    {!! Form::label('name', 'Nome Completo', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('name', null)!!}

    {!! Form::label('data', 'Data de Nascimento:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
                <input type="date" name="data" id="contact_data_nasc">

    {!! Form::label('email', 'E-mail:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::email('email', Input::old('email'))!!}

    {!! Form::label('endereco', 'Endereço:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('endereco', null)!!}


    {!! Form::label('cidade', 'Cidade:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('cidade', null)!!}

    {!! Form::label('estado', 'Estado:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('estado', null)!!}

    {!! Form::label('pais', 'País:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('pais', null)!!}

    {!! Form::label('cpf', 'CPF:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('cpf', null)!!}

    {!! Form::label('rg', 'RG:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
        {!! Form::text('rg', null)!!}

    <input name="_token" type="hidden" value="{{csrf_token()}}"/>

    <div class="">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Editar dados</button>
        </div>
    </div>
    {!! Form::close() !!}


@endsection