@extends('layout')

@section('conteudo')
<table class="card-panel teal black white-text"">
    <tbody">
        <th>Nome</th>
        <th>Data de Nascimento</th>
        <th>Email</th>
        <th>Endereço</th>
        <th>Cidade</th>
        <th>Estado</th>
        <th>País</th>
        <th>CPF</th>
        <th>RG</th>
    </tbody>
<br>
    <tbody class="card-panel teal lighten-2">
        <td>{{$cadastrar->name}}</td>
        <td>{{$cadastrar->data}}</td>
        <td>{{$cadastrar->email}}</td>
        <td>{{$cadastrar->endereco}}</td>
        <td>{{$cadastrar->cidade}}</td>
        <td>{{$cadastrar->estado}}</td>
        <td>{{$cadastrar->pais}}</td>
        <td>{{$cadastrar->cpf}}</td>
        <td>{{$cadastrar->rg}}</td>
    </tbody>

</table>

@endsection