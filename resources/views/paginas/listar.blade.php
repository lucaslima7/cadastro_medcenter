@extends('layout')

@section('conteudo')
<h1 class="card-panel teal blue white-text"; style="font-size: 2.5em; text-align: center;">Listando</h1>

<table>
    <thread >
        <th>Nome</th>
        <th>E-mail</th>
        <th>Ação</th>
    </thread>
    @foreach($listar as $lista)
        <tbody class="card-panel grey lighten-2 ">
        <th>{{$lista->name}}</th>
        <th>{{$lista->email}}</th>
        <th>
            <a href="/show/{{$lista->id}}" class="waves-effect waves-light btn">Ver</a>
            <a href="/edit/{{$lista->id}}" class="waves-effect waves-light blue btn">Editar</a>
            <a href="/delete/{{$lista->id}}" class="waves-effect waves-light red btn">Delete</a>

        </th>
    @endforeach
</table>

@endsection