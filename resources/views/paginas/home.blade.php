@extends('layout')

@section('conteudo')
    {!! Form::open(['url' => '/cadastrar/cliente', 'class' =>'col-sm-12 form-horizontal main_contato', 'method' => 'post', 'role' => 'form'])!!}
    <div class="black-text">Formulário de Cadastro</div>
    <br>

    {!! Form::label('name', 'Nome Completo', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('name', "Digite seu nome",['class'=> 'color'])!!}

    {!! Form::label('data', 'Data de Nascimento:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    <input type="date" name="data" id="contact_data_nasc" class="color">
    {{--{{  date('d/m/Y, strtotime($cliente->tb_data_tb_nascimento ) ) }}--}}

    {!! Form::label('email', 'E-mail:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::email('email', 'Digite seu email',['class'=> 'color'])!!}

    {!! Form::label('endereco', 'Endereço:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('endereco', "Digite seu endereço",['class'=> 'color'])!!}


    {!! Form::label('cidade', 'Cidade:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('cidade', 'Digite sua cidade',['class'=> 'color'])!!}

    {!! Form::label('estado', 'Estado:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('estado', 'Digite seu Estado',['class'=> 'color'])!!}

    {!! Form::label('pais', 'País:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('pais', 'Digite seu país',['class'=> 'color'])!!}

    {!! Form::label('cpf', 'CPF:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('cpf', 'Digite seu CPF',['class'=> 'color'])!!}

    {!! Form::label('rg', 'RG:', ['class' => 'sr-only blue-text text-darken-2 size1 fl-left']) !!}
    {!! Form::text('rg', 'Digite seu RG',['class'=> 'color'])!!}

    <input name="_token" type="hidden" value="{{csrf_token()}}"/>

    <div class="">
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </div>
    {!! Form::close() !!}


@endsection